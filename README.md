# README #

### What is this repository for? ###

This repository is for the UC Berkley Pacman Projects, where solutions are provided for Project 1, Project 2 and Project 3, which are written in Python.

P.S. If you are currently given the UC Berkeley Pacman Project as a project in university, then it is not advised to view or copy the code in this repository as it would be considered as plagiarism.

### How do I get set up? ###

This project was developed using Eclipse PyDev. In order to run different tasks, different arguments were provided to run that specific task.
Visit UC Berkeley's website to view the options and arguments needed to run the programs.

Project 1: http://inst.eecs.berkeley.edu/~cs188/pacman/search.html

Project 2: http://inst.eecs.berkeley.edu/~cs188/pacman/multiagent.html

Project 3: http://inst.eecs.berkeley.edu/~cs188/pacman/reinforcement.html


### Who do I talk to? ###

This project belongs to UC Berkely and was introduced as an assignment through RMIT. The solution was entirely developed by Mohamad Mustafa (mohamadmu95@gmail.com)